from distutils.core import setup

setup(name='fraction_calculator',
  version='1.0',
  py_modules=['calc'],
  scripts=['calc.py']
)
