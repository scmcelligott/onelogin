### Install
Install the script by running the following command from the root directory of this repo:
```
$ sudo python3 setup.py install
```

### Run
The program name is calc.py.  As stated in the help, you will need to wrap your expression in a string, like so:
```
$ calc.py "-1/3 * -2/3"
```