#!/usr/bin/env python

import sys
import math

def evaluate_expression(arguments):
    """
    Return a mixed fraction as the result of performing an operation specified in arguments list
    """
    if len(arguments) != 3:
        print('Program expected 1 argument in the form of 3 items: \'operand operator operand\'' \
               ' and received ' + str(len(arguments)) + ' items: ' + str(arguments) + '; exiting')
        return

    # Convert string representation to a tuple of (numerator, denominator) int values
    frac_1_tuple = None
    frac_2_tuple = None
    try:
        frac_1_tuple = get_numerator_denominator(arguments[0])
        frac_2_tuple = get_numerator_denominator(arguments[2])
    except ValueError as e:
        print(str(e) + '; exiting')
        return

    # Get results of operation
    result = None
    if arguments[1] == '+':
        result = add_fractions(frac_1_tuple, frac_2_tuple)
    elif arguments[1] == '-':
        result = subtract_fractions(frac_1_tuple, frac_2_tuple)
    elif arguments[1] == '*':
        result = multiply_fractions(frac_1_tuple, frac_2_tuple)
    elif arguments[1] == '/':
        result = divide_fractions(frac_1_tuple, frac_2_tuple)
    else:
        print('Second argument (after program name) was not a valid operator (+,-,*,/):' \
                + str(arguments[1]) + '; exiting')
        return
    
    # Format results into a mixed fraction
    return format_fraction(result)


def add_fractions(frac_1_tuple, frac_2_tuple):
    new_numerator = (frac_1_tuple[0] * frac_2_tuple[1]) + (frac_2_tuple[0] * frac_1_tuple[1])
    new_denominator = frac_1_tuple[1] * frac_2_tuple[1]
    return (new_numerator, new_denominator)

def subtract_fractions(frac_1_tuple, frac_2_tuple):
    new_numerator = (frac_1_tuple[0] * frac_2_tuple[1]) - (frac_2_tuple[0] * frac_1_tuple[1])
    new_denominator = frac_1_tuple[1] * frac_2_tuple[1]
    return (new_numerator, new_denominator)


def multiply_fractions(frac_1_tuple, frac_2_tuple):
    new_numerator = frac_1_tuple[0] * frac_2_tuple[0]
    new_denominator = frac_1_tuple[1] * frac_2_tuple[1]
    return (new_numerator, new_denominator)


def divide_fractions(frac_1_tuple, frac_2_tuple):
    new_numerator = frac_1_tuple[0] * frac_2_tuple[1]
    new_denominator = frac_1_tuple[1] * frac_2_tuple[0]
    return (new_numerator, new_denominator)


def format_fraction(frac_tuple):
    """
    Return a stringified mix fraction, calculated from frac_tuple, whose first value 
    represents a fraction's numerator, and whose second value represents a fraction's denominator
    """

    # Check to see if result should be negative; if it is negative, set a sign variable to '-'
    # We will prepend this string to the front of the fraction we are returning
    sign = ''
    if (frac_tuple[0] < 0) != (frac_tuple[1] < 0):
        sign = '-'
        
    frac_tuple = (abs(frac_tuple[0]), abs(frac_tuple[1]))

    # Get a whole number , if there is one
    whole_number = math.floor(frac_tuple[0] / frac_tuple[1])

    # Get the numerator's  remainder after pulling out the whole number.  
    # If the remainder is 0, return the whole number since we don't have a fraction to add at the end
    remainder_numerator = math.floor(frac_tuple[0] % frac_tuple[1])
    if remainder_numerator is 0:
        return sign + str(whole_number)

    # Reduce the fraction such that 24/48 becomes 1/2
    remainder_denominator = frac_tuple[1]
    reduced_frac_tuple = reduce_fraction((remainder_numerator, remainder_denominator))

    if whole_number > 0:
        return sign + str(whole_number) + '_' + str(reduced_frac_tuple[0]) + '/' + str(reduced_frac_tuple[1])
    else:
        return sign + str(reduced_frac_tuple[0]) + '/' + str(reduced_frac_tuple[1])


def reduce_fraction(frac_tuple):
    """
    Returns a tuple representation of a fraction that has been reduced.  So if you pass in
    (7,21), you will get a return value of (1,3)
    """
    numerator = frac_tuple[0]
    denominator = frac_tuple[1]
    
    i = numerator
    while i > 1:

        # If numerator and denominator are divisible by this number, divide each of them by the number and continue
        if numerator % i == 0 and denominator % i == 0:
            numerator = numerator / i
            denominator = denominator / i
            i = numerator
        else:
            i = i - 1

    return (int(numerator), int(denominator))


def get_numerator_denominator(fraction):
    """
    Return the fraction's numerator and denominator values as a tuple
    """

    whole_num = None
    whole_num_end_idx = 0
    try:
        # The following line will throw an exception if string is not found
        whole_num_end_idx = fraction.index('_')
        whole_num_str = fraction[:whole_num_end_idx]
        whole_num = int(whole_num_str)
        whole_num_end_idx += 1
    except:
        # Could be the case that we just have a fraction or whole number
        # Check to see if it's just a whole number
        try:
            whole_num = int(fraction)
        except:
            # Must be a fraction on the end; we can pass here
            pass

    fraction_numerator = 0
    fraction_denominator = 1
        
    fraction_idx = fraction.find('/')
    if (fraction_idx < 0):
        # We could end up here if 1) we are just looking at a whole number or 2) we have bad input
        if whole_num is None:
            # We don't have a whol_num AND don't have a fraction; bad input
            raise ValueError('fraction passed into get_numerator_denominator (' + str(fraction) + ')' \
                    + ' is not a valid fraction')
    else:
        try:
            fraction_numerator = int(fraction[whole_num_end_idx:fraction_idx])
            fraction_denominator = int(fraction[(fraction_idx+1):])
        except ValueError as e:
            # Parsing error; invalid input
            raise ValueError('error parsing numerator or denominator for fraction (' \
                    + str(fraction) + '): ' + str(e))

    if fraction_denominator == 0:
        raise ValueError('fraction passed into get_numerator_denominator (' + str(fraction) + ')' \
                + ' is not a valid fraction; has 0 as a denominator')

    if whole_num is None:
        whole_num = 0

    numerator = whole_num * fraction_denominator + fraction_numerator
    return (numerator, fraction_denominator)


def main():
    
    if len(sys.argv) != 2:
        print('calc.py requires 1 additonal argument in the form of string with format ' \
                + '\'operand operator operand\'; received ' + str(len(sys.argv)-1) \
                + ' additional arguments: ' + str(sys.argv[1:]) + '; exiting')
        sys.exit()

    if sys.argv[1] == '--help' or sys.argv == '-h':
        print('\ncalc.py requires 1 additonal argument in the form of string with format \n' \
        + '\'operand operator operand\' where each operand is a fraction, either mixed \n' \
        + '(1_3/4) or irregular (7\/4) in the format <whole_num>_<numerator>/<denominator>.\n' \
        + 'The two fraction operands are separated by an operator (+, -, *, -).\n' \
        + '\tEXAMPLE: $ calc.py \'1_3/4 * -8/3\'  ===> -4_2/3\n')
        sys.exit()

    # First argument is the name of the script; second argument is of format 
    # "operand operation operand" with quotes around the expression
    # Split the incoming string by spaces such that you pass in a list of strings
    expression_parts = sys.argv[1].split()
    result = evaluate_expression(expression_parts)
    print(result)


if __name__ == "__main__":
    main()